<?php
/**
 * Plugin Name:       Modal Window
 * Plugin URI:        https://wordpress.org/plugins/modal-window/
 * Description:       This plugin is for creation of modal windows!
 * Version:           1.3
 * Author:            Wow Company
 * Author URI:        http://wow-company.com
 * License:           GPL-2.0+
 * License URI:       http://www.gnu.org/licenses/gpl-2.0.txt
 * Text Domain:       umw
  */
if ( ! defined( 'WPINC' ) ) {die;}
define( 'WOW_MODALSIMPLE_PLUGIN_BASENAME', dirname(plugin_basename(__FILE__)) );
load_plugin_textdomain('umw', false, dirname(plugin_basename(__FILE__)) . '/languages/');
function activate_wow_modalsimple() {
	require_once plugin_dir_path( __FILE__ ) . 'includes/activator.php';	
}	
register_activation_hook( __FILE__, 'activate_wow_modalsimple' );
function deactivate_wow_modalsimple() {	
	require_once plugin_dir_path( __FILE__ ) . 'includes/deactivator.php';
}
register_deactivation_hook( __FILE__, 'deactivate_wow_modalsimple' );

if( !class_exists( 'JavaScriptPacker' )) {
	require_once plugin_dir_path( __FILE__ ) . 'includes/class.JavaScriptPacker.php';
}
if( !class_exists( 'WOWWPClass' )) {
	require_once plugin_dir_path( __FILE__ ) . 'includes/wowclass.php';
}
require_once plugin_dir_path( __FILE__ ) . 'admin/admin.php';
require_once plugin_dir_path( __FILE__ ) . 'public/public.php';

function wow_modalsimple_row_meta( $meta, $plugin_file ){
	if( false === strpos( $plugin_file, basename(__FILE__) ) )
		return $meta;

	$meta[] = 'Support: <a href="https://www.facebook.com/wowaffect/" target="_blank">Facebook</a>';
	return $meta; 
}
add_filter( 'plugin_row_meta', 'wow_modalsimple_row_meta', 10, 4 );

function wow_modalsimple_action_links( $actions, $plugin_file ){
	if( false === strpos( $plugin_file, basename(__FILE__) ) )
		return $actions;

	$settings_link = '<a href="admin.php?page=wow-modalsimple' .'">Settings</a>'; 
	array_unshift( $actions, $settings_link ); 
	return $actions; 
}
add_filter( 'plugin_action_links', 'wow_modalsimple_action_links', 10, 2 );