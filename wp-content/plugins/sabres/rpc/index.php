<?php

// Disable time limit
@ini_set( 'max_execution_time', '0' );
@set_time_limit( 0 );

// Disable memory limit
@ini_set( 'memory_limit', '-1' );



define( 'SBS_RPC', true );
define('SABRES_PLUGIN_DIR',realpath(__DIR__.'/../'));
$sabres_config_exists=@include_once SABRES_PLUGIN_DIR.'/sabres-config.php';
if ($sabres_config_exists) {
  define('ABSPATH',Sabres_Config::get('ABSPATH'));
  define('WP_CONTENT_DIR',Sabres_Config::get('WP_CONTENT_DIR'));
}
else {
  define('ABSPATH',preg_replace( '/wp-content(?!.*wp-content).*/', '', __DIR__ ));
  if (!file_exists(ABSPATH))
        die('Could not find ABSPATH');
  define('WP_CONTENT_DIR',ABSPATH.'wp-content');
}
define('SABRES_PLUGIN_MAIN_FILE',SABRES_PLUGIN_DIR.'/sabress.php');
define('SABRES_PLUGIN_BASE_NAME',str_replace(realpath(SABRES_PLUGIN_DIR.'/../').'/','',SABRES_PLUGIN_MAIN_FILE));
date_default_timezone_set('UTC');
require_once SABRES_PLUGIN_DIR . '/library/fail.php';
require_once SABRES_PLUGIN_DIR.'/library/cache.php';
SBS_Cache::disable_cache();


Class RPC_Dispatcher {

  private $argsParserCallback;
  private $messageContentProvider;
  private $outputFinalized=false;
  private $opHandlers;

  function __construct($argsParserCallback,$messageContentProvider) {
    if (!isset($argsParserCallback))
        $argsParserCallback=array($this,'parseRequest');
    $this->argsParserCallback=$argsParserCallback;
    if (!isset($messageContentProvider))
        $messageContentProvider=array($this,'getMessageContent');
    $this->messageContentProvider=$messageContentProvider;
    $this->opHandlers=array();
    $this->opHandlers['activate']=array('require'=>'activate_plugin','class'=>'Activate_Plugin');
    $this->opHandlers['wpActivate']=array('require'=>'wp_activate_plugin','class'=>'WP_Activate_Plugin');
    $this->opHandlers['wpIsActive']=array('require'=>'wp_is_plugin_active','class'=>'WP_Is_Plugin_Active');
    $this->opHandlers['forceActivate']=array('require'=>'force_activate_plugin','class'=>'Force_Activate_Plugin');
    $this->opHandlers['getLog']=array('require'=>'get_log','class'=>'Get_Log');
    $this->opHandlers['getSettings']=array('require'=>'get_settings','class'=>'Get_Settings');
    $this->opHandlers['dirIt']=array('require'=>'dir_it','class'=>'Dir_It');
    $this->opHandlers['clearLog']=array('require'=>'clear_log','class'=>'Clear_Log');
    $this->opHandlers['settings']=array('require'=>'set_settings','class'=>'Set_Settings');
    $this->opHandlers['coreInventory']=array('require'=>'core_inventory','class'=>'Core_Inventory');
    $this->opHandlers['pluginsInventory']=array('require'=>'plugins_inventory','class'=>'Plugins_Inventory');
    $this->opHandlers['themesInventory']=array('require'=>'themes_inventory','class'=>'Themes_Inventory');
    $this->opHandlers['themesInventory2']=array('require'=>'themes_inventory2','class'=>'Themes_Inventory2');
    $this->opHandlers['updatePlugin']=array('require'=>'update_plugin','class'=>'Update_Plugin');
    $this->opHandlers['updatePlugin2']=array('require'=>'update_plugin_2','class'=>'Update_Plugin_2');
    $this->opHandlers['firewallSettings']=array('require'=>'firewall_settings','class'=>'Firewall_Settings');
    $this->opHandlers['performMalwareScan']=array('require'=>'malware_scan','class'=>'Malware_Scan');
    $this->opHandlers['performSecurityScan']=array('require'=>'security_scan','class'=>'Security_Scan');
    $this->opHandlers['performSecurityFix']=array('require'=>'security_fix','class'=>'Security_Fix');
    $this->opHandlers['getScans']=array('require'=>'get_scans','class'=>'Get_Scans');
    $this->opHandlers['backup']=array('require'=>'backup','class'=>'Backup');
    $this->opHandlers['uploadBackup']=array('require'=>'upload_backup','class'=>'Upload_Backup');
    $this->opHandlers['getVersion']=array('require'=>'get_version','class'=>'Get_Version');
    $this->opHandlers['getSSLInfo']=array('require'=>'get_ssl_info','class'=>'Get_SSL_Info');
    $this->opHandlers['getSystemInfo']=array('require'=>'get_system_info','class'=>'Get_System_Info');

  }

  public function dispatch() {

    ob_start();
    $this->doDispatch();
    $this->finalizeOutput();

  }

  private function finalizeOutput() {
    //echo "Shutting Down";
    if (!$this->outputFinalized) {
      $this->outputFinalized=true;

      $output=ob_get_contents();
      ob_end_clean();
      if (!$this->unencryptedResponse) {
        require_once SABRES_PLUGIN_DIR.'/_inc/settings.php';
        require_once SABRES_PLUGIN_DIR.'/_inc/sbr_utils.php';
        require_once SABRES_PLUGIN_DIR.'/library/crypto.php';
        require_once ABSPATH.'/wp-load.php';

        $settings=SbrSettings::instance();

        if ( strlen( $settings->symmetricEncryptionKey ) == 40 ) {
            $aes_key = substr( $settings->symmetricEncryptionKey, 0, 32 );
            $aes_iv = substr( $settings->symmetricEncryptionKey, 24, 16 );
            $output=SBS_Crypto::encrypt($aes_key,$aes_iv,$output);
        }
      }
      echo $output;
    }
  }

  public function onShutDown() {
    $this->finalizeOutput();
  }

  private $unencryptedResponse=false;

  public function doDispatch() {
    if (!extension_loaded('openssl'))
      SBS_Fail::bye("Open SSL extension is not loaded");
    $args=call_user_func($this->argsParserCallback);
    if (!isset($args))
       return;



    $messageContent=call_user_func($this->messageContentProvider,$args["message-id"]);
    $parsedMessage=self::parse_message($messageContent);
    if (!isset($parsedMessage))
      SBS_Fail::bye("Failed to parse message: ".$messageContent);
    if (!isset($parsedMessage['op']))
      SBS_Fail::bye("Missing mandatory parameter op. message: ".$messageContent);
    if (isset($parsedMessage['unencryptedResponse']) && strcasecmp($parsedMessage['unencryptedResponse'],true))
      $this->unencryptedResponse=true;

    $handler=@$this->opHandlers[$parsedMessage['op']];
    if (!isset($handler))
      SBS_Fail::byeArr(array( 'message'=>"Unkown op: ".$parsedMessage['op'],
                              'code'=>400
                             ));
    require_once $handler['require'].'.php';

    $handlerInstance=new $handler['class'];
    $handlerInstance->execute($parsedMessage);

  }



  public function parseRequest() {
    if (!isset($_GET) || empty($_GET) || !isset($_GET["message-id"])) {
      header('HTTP/1.1 500 Internal Server Error');
      return null;
    }
    return array('message-id'=>$_GET["message-id"]);
  }

  public function getMessageContent($messageID) {
    if (function_exists('curl_init') && function_exists('curl_setopt') && function_exists('curl_exec') && function_exists('curl_error')
       && function_exists('curl_getinfo') && function_exists('curl_close'))
       return $this->getMessageContentViaCurlLib($messageID);
    if (function_exists('fopen') && function_exists('stream_context_create'))
      return $this->getMessageContentViaStreams($messageID);
    return $this->getMessageViaWPRemotePost($messageID);
    SBS_Fail::bye('Failed to find any available http service');


  }

  //this is a duplicate of function in net.php, for sake of indepedance of this ,module it was duplicated
  public function get_real_ip_address()
  {
      $server_attributes = array( 'HTTP_CLIENT_IP',
          'HTTP_X_FORWARDED_FOR',
          'HTTP_X_FORWARDED',
          'HTTP_FORWARDED_FOR',
          'HTTP_FORWARDED',
          'REMOTE_ADDR'
      );

      foreach ( $server_attributes as $attr )
          if ( !empty( $_SERVER[$attr] ) )
              return $_SERVER[$attr];
  }

  private static function getPostURL() {
    return 'https://sa-gateway.sabressecurity.com/round-trip-wppp';
  }

  private function getMessageViaWPRemotePost($messageID) {
    require_once ABSPATH.'/wp-load.php';


    $url = self::getPostURL();
    $params=array('code'=>$messageID,'sourceIP'=>$this->get_real_ip_address());

    $res = wp_remote_post($url, array(
        'method' => 'POST',
        'timeout' => 45,
        'redirection' => 5,
        'httpversion' => '1.0',
        'blocking' => true,
        'headers' => array(),
        'sslverify' => false,
        'body' => $params,
        'cookies' => array()
    ));

    if (!is_wp_error($res)) {
        if (!isset($res['response']) || !isset($res['response']['code']) || $res['response']['code']!=200) {
          $message='Request to '.$url.' failed. Response Code: '.$res['response']['code'];
          if (isset($res['response']['message'])) {
            $message=$message.'. Message: '.$res['response']['message'];
          }
          unset($res['response']);
          SBS_Fail::bye($message,$res);
        }
        if (isset($res['body'])) {
            return $res['body'];
        }
    } else {
        SBS_Fail::bye($res->get_error_message(),$res->get_error_data());
    }
  }

  private function getMessageContentViaStreams($messageID) {
      $url = self::getPostURL();
      $params=array('code'=>$messageID,'sourceIP'=>$this->get_real_ip_address());
      $params=http_build_query($params);

      $context_options=array(
        'http'=>array(
          'method' => 'POST',
          'header'=> "Content-type: application/x-www-form-urlencoded\r\n"
                . "Content-Length: " . strlen($params) . "\r\n",
            'content' => $params
        ),
        'ssl'=>array(
          'verify_peer' => false,
          'verify_peer_name'=> false,
          'allow_self_signed'=>true
        )

      );
      $context = stream_context_create($context_options);
      $stream = @fopen($url, 'r', false, $context);
      if ($stream===False) {
        $error=error_get_last();
        if (isset($error['message'])) {
          $message=$error['message'];
          unset($error['message']);
        }
        else {
          $message="Request failed";
        }
        SBS_Fail::bye($message,$error);
      }
      $data=stream_get_contents($stream);

      fclose($stream);

      return $data;



  }


  private function getMessageContentViaCurlLib($messageID) {
    $url = self::getPostURL();
    $params=array('code'=>$messageID,'sourceIP'=>$this->get_real_ip_address());


    $ch = curl_init();

    if (FALSE === $ch)
      SBS_Fail::bye('failed to initialize');

    curl_setopt($ch, CURLOPT_URL,$url);
    curl_setopt($ch, CURLOPT_POST, 1);
    curl_setopt($ch, CURLOPT_POSTFIELDS,http_build_query($params));

    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch,CURLOPT_SSL_VERIFYHOST,0);
    curl_setopt($ch,CURLOPT_SSL_VERIFYPEER,false);


    $server_output = curl_exec ($ch);

    if (FALSE === $server_output)
        SBS_Fail::bye(curl_error($ch));

    if (curl_getinfo($ch,CURLINFO_HTTP_CODE)!=200) {
      SBS_Fail::bye("Request to ".$url.' failed. Response code : '.curl_getinfo($ch,CURLINFO_HTTP_CODE),null);
    }

    curl_close ($ch);

    return $server_output;

  }


  public static function parse_message( $message )
  {
      $ret = array();
      $urlParts = explode( '&', $message );

      foreach ( $urlParts as $part ) {
          $segments = explode( '=', $part, 2 );
          $value=null;
          if (isset($segments[1]))
             $value=$segments[1];

          if ( count( $segments ) > 1 ) {
              $ret[$segments[0]] = $value;
          }
      }

      return $ret;
  }

}

$dispatcher=new RPC_Dispatcher(null,null);
register_shutdown_function(array($dispatcher,'onShutDown'));
$dispatcher->dispatch();
