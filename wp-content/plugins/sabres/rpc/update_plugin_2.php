<?php
define('FS_METHOD','direct');
require_once SABRES_PLUGIN_DIR . '/library/fail.php';
require_once SABRES_PLUGIN_DIR . '/_inc/sbr_utils.php';
require_once ABSPATH.'/wp-load.php';
require_once ( ABSPATH . '/wp-admin/includes/admin.php' );
require_once ( ABSPATH . '/wp-admin/includes/class-wp-upgrader.php' );

class Sabres_InstallHandler extends Plugin_Installer_Skin {

  var $feedback;
  var $error;

  function error( $error ) {
  $this->error = $error;
  }

  function feedback( $feedback ) {
  $this->feedback = $feedback;
  }

  function before() { }

  function after() { }

  function header() { }

  function footer() { }

  function request_filesystem_credentials( $error = false, $context = '', $allow_relaxed_file_ownership = false ) {
      return parent::request_filesystem_credentials($error,$context,true);
  }

}

class Update_Plugin_2 {
    public function execute($rpc_data) {
      $filename = null;

      if ( isset( $rpc_data['fileName'] ) && $rpc_data['fileName'] != '' ) {
          $filename = $rpc_data['fileName'];
      }

      if (!isset($filename) || empty($filename))
        SBS_Fail::byeArr(array( 'message'=>"file name must have a value",
                           'code'=>500
                          ));
      $download_url=SbrUtils::trgx('plugin_download_url');
      if (!isset($download_url) || empty($download_url))
        SBS_Fail::byeArr(array( 'message'=>"Download URL must have a value",
                           'code'=>500
                          ));
      $result=$this->do_update_plugin('sabres/sabress.php',$download_url.'/'.$filename);
      if (is_wp_error($result))
        SBS_Fail::byeArr(array( 'message'=>"Failed to update plugin. ".$result->get_error_message(),
                             'code'=>500,
                             'logData'=>$result->get_error_data()
                            ));
       else
           echo $result;

    }

    function mockPendingUpdatesFilter() {
    	global $global_update_info;

    	$current = new stdClass;
    	$current->response = array();
    	$plugin_file = $global_update_info['plugin_file'];
    	$current->response[$plugin_file] = new stdClass;
    	$current->response[$plugin_file]->package = $global_update_info['package'];


    	return $current;
    }

    function do_update_plugin( $plugin_file, $zip_url ) {
    	global $global_update_info;

    	if ( defined( 'DISALLOW_FILE_MODS' ) && DISALLOW_FILE_MODS )
    		return new WP_Error( 'disallow-file-mods', __( "File modification is disabled with the DISALLOW_FILE_MODS constant.", 'wpremote' ) );
    	$is_active         = is_plugin_active( $plugin_file );
    	$is_active_network = is_plugin_active_for_network( $plugin_file );

    	foreach( get_plugins() as $path => $maybe_plugin ) {
    		if ( $path == $plugin_file ) {
    			$plugin = $maybe_plugin;
    			break;
    		}
    	}

    	$skin = new Sabres_InstallHandler();
    	$upgrader = new Plugin_Upgrader( $skin );

        $global_update_info = array(
    			'plugin_file'    => $plugin_file,
    			'package'        => $zip_url,
    		);
    		add_filter( 'pre_site_transient_update_plugins', array($this,'mockPendingUpdatesFilter') );



    	// Do the upgrade
    	ob_start();
    	$result = $upgrader->upgrade( $plugin_file );
        $data = ob_get_contents();
    	ob_clean();

    	if ( ! empty( $skin->error ) )
            return $skin->error;
    	else if ( is_wp_error( $result ) )
    		return $result;
        else if ( ( ! $result && ! is_null( $result ) ))
    		return new WP_Error( 'plugin-update', __( 'Unknown error updating plugin.', 'Sabres Version Update' ),$data );
    	if ( $is_active )
    		activate_plugin( $plugin_file, '', $is_active_network, true );

    	return $skin->feedback;
    }

}
