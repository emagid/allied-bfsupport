(function( $ ) {
    $.fn.SabresServices = function(options, modFirewall, addedInPortal) {

        var boxes = {
            'website-hardening': function(services) {
                return services['security_scanner'] ? services['security_scanner'].active == "1" : false;
            },
            'firewall-connection': function() {
                return modFirewall;
            },
            'complete-malware-scan': function(services) {
                var integrity = services['integrity_scanner'] ? services['integrity_scanner'].active == "1" : false;
                var malware = services['malware_scanner'] ? services['malware_scanner'].active == "1" : false;

                return malware && integrity;
            },
            'scans-alerts-timing': function(services) {
                var integrity = services['integrity_scanner'] ? services['integrity_scanner'].active == "1" : false;
                var malware = services['malware_scanner'] ? services['malware_scanner'].active == "1" : false;
                var vulnerabilities = services['vulnerabilities_scanner'] ? services['vulnerabilities_scanner'].active == "1" : false;

                return integrity || malware || vulnerabilities;
            },
            'firewall-system-management': function() {
                return addedInPortal && modFirewall;
            },
            'multiple-website-dashboard': function() {
                return addedInPortal;
            },
            'complete-daily-backup': function(services) {
                return services['backup'] ? services['backup'].active == "1" : false;
            },
            'vulnerability-management': function(services) {
                return services['vulnerabilities_scanner'] ? services['vulnerabilities_scanner'].active == "1" : false;
            },
            'two-factor-authentication': function(services) {
                return services['two_factor_authentication'] ? services['two_factor_authentication'].active == "1" : false;
            },
            'defacement-alerts': function(services) {
                var homepage = services['homepage_snapshot_monitor'] ? services['homepage_snapshot_monitor'].active == "1" : false;
                var defacement = services['defacement_monitor'] ? services['defacement_monitor'].active == "1" : false;

                return homepage && defacement;
            }
        };

        var Sabres = window.Sabres.noConflict();

        Sabres.pluginAuth(options, function(api) {
            api.get("services").done(function(res) {
                var services = {};
                res.forEach(function(service) {
                    var name = service.name.split(" ").join("_").toLowerCase();
                    services[name] = service;
                });
                for(var box in boxes) {
                    var $box = $("[data-box='" + box + "']");
                    if ( $box.length ) {
                        if ( boxes[box](services) ) {
                            $box.removeClass().addClass("sbr-box sbr-box-green");
                        }
                    }
                }
            })
        });
    };
})(jQuery);
