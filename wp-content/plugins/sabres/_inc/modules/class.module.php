<?php
// Exit if accessed directly
if ( !defined( 'ABSPATH' ) ) {
    exit;
}


if ( !class_exists( 'SBS_Module' ) ) {
    require_once SABRES_PLUGIN_DIR . '/_inc/class.singleton.php';
    require_once SABRES_PLUGIN_DIR.'/_inc/event.aware.php';
    require_once SABRES_PLUGIN_DIR.'/_inc/class.event.manager.php';

    abstract class SBS_Module extends SBS_Singleton implements SBS_EventAwareInterface
    {
        public function init(SBS_EventManager $eventManager) {
            $this->register_events($eventManager);
        }
    }
}