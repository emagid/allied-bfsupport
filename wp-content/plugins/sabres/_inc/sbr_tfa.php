<?php


class SbrTfa
{
    const DELIVERY_TYPE_EMAIL = 'email';
    const DELIVERY_TYPE_SMS = 'sms';
    const DELIVERY_TYPE_BOTH = 'both';

    const USER_META_KEY = 'tfa';

    const PHONE_PATTERN = '/^\+?[0-9]{7,}$/';

    const STRICTNESS_TYPE_NEW_DEVICE='new-device';
    const STRICTNESS_TYPE_EVERY_LOGIN='every-login';

    public static function get_settings(WP_User $user) {
        $settings = SbrUtils::get_user_meta( $user->ID, static::USER_META_KEY, true);
        $settings = json_decode( $settings, true );

        if ($settings==null)
          $settings=array();
        $settings =array_merge(static::get_default_settings( $user ),$settings);

        return $settings;
    }

    public static function update_settings(WP_User $user, $settings) {

        $errors = static::validate( $settings );

        if( count( $errors ) == 0) {
            $settings = static::prepare( $settings );
            SbrUtils::update_user_meta( $user->ID, static::USER_META_KEY, SbrUtils::get_json( $settings ) );
        }

        return $errors;
    }

    protected static function validate(&$settings) {
        $errors = array();

        $delivery_types = array(
            static::DELIVERY_TYPE_EMAIL,
            static::DELIVERY_TYPE_SMS,
            static::DELIVERY_TYPE_BOTH
        );


        if( !array_key_exists( 'delivery', $settings ) || !in_array( $settings['delivery'], $delivery_types ) ) {
            $errors[] = "Invalid token delivery";
        }

        $strictness_types=array(static::STRICTNESS_TYPE_NEW_DEVICE,static::STRICTNESS_TYPE_EVERY_LOGIN);

        if( !array_key_exists( 'strictness', $settings ) || !in_array( $settings['strictness'], $strictness_types ) ) {
            $errors[] = "Invalid token strictness";
        }

        if( $settings['delivery'] == static::DELIVERY_TYPE_BOTH || $settings['delivery'] == static::DELIVERY_TYPE_EMAIL) {
            if( !array_key_exists( 'email', $settings ) || !is_email( $settings['email'] ) ) {
                $errors[] = "Invalid email";
            }
        }

        if( $settings['delivery'] == static::DELIVERY_TYPE_BOTH || $settings['delivery'] == static::DELIVERY_TYPE_SMS) {

            $number_str = isset( $settings['smsNumber'] ) ? $settings['smsNumber'] : '';
            if( !preg_match( static::PHONE_PATTERN , $number_str ) ) {
                $errors[] = "Invalid phone number";
            }

        }

        if (!array_key_exists('device-expiry-checked',$settings))
              $errors[] = "Device expiry checked can not be empty";
        else if (!SbrUtils::is_true_of_false($settings['device-expiry-checked'],$result))
              $errors[] = "Invalid value for device expirty checked ".var_export($settings['device-expiry-checked'],true);
        else
            $settings['device-expiry-checked']=$result;

        if (!array_key_exists('device-expiry-days',$settings))
              $errors[] = "Device expiry days can not be empty";
        else if (!SbrUtils::is_integer($settings['device-expiry-days'],$result))
              $errors[] = "Invalid value for device expiry days. Expected integer got: ".var_export($settings['device-expiry-days'],true);
        else if ($result<1 || $result>1000)
            $errors[] = "Invalid value for device expiry days. Value should be between 1 and 1000";
        else
            $settings['device-expiry-days']=$result;




        return $errors;
    }

    protected static function prepare($settings) {
        if( $settings['delivery'] == static::DELIVERY_TYPE_EMAIL ) {
            $settings['smsNumber'] = '';
        }

        if( $settings['delivery'] == static::DELIVERY_TYPE_SMS ) {
            $settings['email'] = '';
        }

        return $settings;
    }

    protected static function get_default_settings(WP_USER $user) {
        return array(
            'delivery' => static::DELIVERY_TYPE_EMAIL,
            'email' => $user->user_email,
            'strictness' => static::STRICTNESS_TYPE_NEW_DEVICE,
            'device-expiry-checked' => false,
            'device-expiry-days' => 2
        );
    }
}
