<?php

require_once SABRES_PLUGIN_DIR.'/library/hash.php';
if (!class_exists('SbrUtils') && defined('SABRES_PLUGIN_DIR'))
    require_once SABRES_PLUGIN_DIR.'/_inc/sbr_utils.php';
if (!function_exists('get_option') && defined('ABSPATH'))
    require_once ABSPATH.'/wp-load.php';


///
/// Created: 2016-01-06
/// By: Ronny Sherer
/// Purpose: Sabres settings class. It stores the settings in a PHP file.
///          Settings are loaded by include_once.
///          Fallback is saved in the wordpress options.
///
class SbrSettings
{
    private static $singleton = NULL;
    private $settingsOption = 'sbr-settings';
    private $settingsFile = 'settings_array.php';
    private static $default_settings=array(
        'websiteSabresServerToken' => '',
        'websiteSabresClientToken' => '',
        'https' => 'True',
        'maxDepth' => 4,
        'isActive' => 'False',
        'addedInPortal' => 'False',
        // 'LicenseType' => NULL, // 'FREE' | 'LIGHT' | 'MEDIUM' | 'HEAVY'
        'debug' => 'False',
        'mod_tfa_active' => 'False',
        'mod_firewall_active' => 'True',
        'mod_firewall_country_mode' => 'allow_all',
        'mod_firewall_xml_rpc' => 'True',
        'token' => '',
        'symmetricEncryptionKey' => '',
        'verifyHashSalt' => '',
        'apiKey' => '',
        'version_number' => SBS_VERSION,
        'click_jacking' => 'True',
        'login_errors' => 'False',
        'author_archive' => 'False',
        'wp_api_keys_url' => 'https://api.wordpress.org/secret-key/1.1/salt/',
        'auto_update' => 'False',
        'error_handling' => 'False',
        'debug_mode' => 'False',
        'force_ssl' => 'False',
        'preInstall' => 'False'
    );
    private $settings;


    private function init() {
      $this->settings=self::$default_settings;
      $this->settings['apiKey'] = SBS_Hash::generate_hash();
      $this->settings['token'] = SBS_Hash::generate_hash();
      $this->settings['symmetricEncryptionKey'] = SBS_Hash::generate_hash();
      $this->settings['verifyHashSalt'] = SBS_Hash::generate_hash();

    }

    /// Construct and load the settings.
    private function __construct()
    {
        $this->init();

        // MU
        if ( defined( 'MULTISITE' ) && MULTISITE && !empty( $GLOBALS['blog_id'] ) ) {
            $blog_id = $GLOBALS['blog_id'];

            $this->settingsFile = sprintf( 'settings_array_%d.php', $blog_id );
        }

        $this->settingsFile = SABRES_PLUGIN_DIR.'/'.$this->settingsFile;
        $this->load_settings();
    }

    static public function instance()
    {
        if (self::$singleton == NULL)
            self::$singleton = new SbrSettings();
        return self::$singleton;
    }
    public static function reload() {
      self::$singleton=NULL;
      return self::instance();

    }
    /////////////////////////////////////////
    // Public:

    /// Get value by key.
    public function __get($key)
    {
        return array_key_exists($key, $this->settings) ? $this->settings[$key] : '';
    }

    /// Set Key's value.
    public function __set($key, $value)
    {
        $this->settings[$key] = $value;

        return $this->store_settings();
    }

    public function should_trigger_activation() {
      return $this->websiteSabresServerToken=='' || $this->websiteSabresClientToken=='';
    }

    /// Set an array of values and store them.
    public function set_values($sets)
    {
        foreach ($sets as $key => $value)
        {
            $this->settings[$key] = $value;
        }
        return $this->store_settings();
    }

    /// Get the settings as a JSON string
    public function get_json($fields = null)
    {
        if ( !empty( $fields ) ) {
            $settings = array_intersect_key ( $this->settings,
                array_flip ( $fields ) );
        } else {
            $settings = $this->settings;
        }

        return SbrUtils::get_json($settings);
    }

    public function get_settings( $prefix = null ) {
        $results = array();

        foreach ( $this->settings as $key => $value ) {
            if ( stripos( $key, $prefix ) === 0 ) {
                $new_key = substr( $key, strlen( $prefix ) + 1 );
                $results[$new_key] = $this->settings[$key];
            }
        }

        return $results;
    }

    public function reset() {
        $this->init();
        $this->store_settings();
        //$this->load_settings();
    }

    /////////////////////////////////////////
    // Private:

    private function load_settings()
    {
//        if (file_exists($this->settingsFile))
//        {
//            include $this->settingsFile;	// Loads array
//
//            $this->settings = array_merge($this->settings, $sbr_settings);
//        }
//        else
//        {
            $this->load_from_wordpress_options();
//        }
    }

    private function load_from_wordpress_options()
    {
        $wp_sbr_options = get_option($this->settingsOption);
        if (empty($wp_sbr_options)){
            $this->store_settings();
            return false;
        }

        $sbr_settings = SbrUtils::obj_to_associated_array( json_decode($wp_sbr_options) );
        if (!empty($sbr_settings))
        {
            $this->settings = array_merge($this->settings, $sbr_settings);
            $this->store_settings();
        }

        return true;
    }

    private function store_settings()
    {
        if (empty($this->settings))
            return false;

        $json = $this->get_json();
        update_option($this->settingsOption, $json );

//        $phpStr = "<?php\n\$sbr_settings = ".$this->php_array_to_str($this->settings).';';
//        if (!file_put_contents($this->settingsFile, $phpStr))
//        {
//            // Log Error
//        }
        return $json;
    }

    private function php_array_to_str($arr)
    {
        $str = '';
        foreach ($arr as $key => $value)
        {
            $str .= "\t'$key' => ";
            $str .= is_array($value) ? $this->php_array_to_str($value) : "'$value'";
            $str .= ",\n";
        }
        return "array(\n$str)";
    }
}
