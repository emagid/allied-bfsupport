<?php


// Exit if accessed directly
if ( !defined( 'ABSPATH' ) ) {
    exit;
}

if ( !class_exists( 'SBS_EventManager' ) ) {
    require_once SABRES_PLUGIN_DIR.'/_inc/class.singleton.php';


    class SBS_EventManager extends SBS_Singleton
    {
        protected $_event_callbacks = [];

        protected static $instance;

        public function register_event_callback( $event_name, $callback ) {
            if ( !isset( $this->_event_callbacks[$event_name] ) ) {
                $this->_event_callbacks[$event_name] = array();
            }

            array_push( $this->_event_callbacks[$event_name], $callback );
        }

        public function event_trigger( $event_name, $args ) {

            if ( !empty( $this->_event_callbacks[$event_name] ) ) {
                $callbacks = $this->_event_callbacks[$event_name];

                foreach ( $callbacks as $callback ) {
                    if ( !empty( $args ) ) {
                        call_user_func_array( $callback, $args );
                    } else {
                        call_user_func( $callback );
                    }
                }
            }
        }
    }
}