<?php
// Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) {
    exit;
}
require_once SABRES_PLUGIN_DIR.'/_inc/settings.php';
require_once SABRES_PLUGIN_DIR.'/library/wp.php';
class Check_Sabres_Config {
  const TEMPLATE_VERSION=1;

  public static function check() {
    $settings=SbrSettings::instance();
    if ($settings->sabres_config_error!=='') {
      return;
    }
    $config_file_path=SABRES_PLUGIN_DIR.'/sabres-config.php';
    $file_exists=@include_once $config_file_path;

    $update_plugin_url=false;
    if ($file_exists) {
      $curr_template_version=0;
      if (method_exists('Sabres_Config','get_version'))
        $curr_template_version=Sabres_Config::get_version();
      if (self::TEMPLATE_VERSION>$curr_template_version || Sabres_Config::any_value_changed()) {        
        if (Sabres_Config::is_plugin_url_changed()) {
          $update_plugin_url=true;
        }
        if (!@unlink($config_file_path)) {
          $err = error_get_last();
          $settings->sabres_config_error=$err['message'];
          return;
        }
        Sabres_Config::reset();
        $config_deleted=true;
      }
    }
    else
        $update_plugin_url=true;
    if ($config_deleted || !$file_exists) {
      $template=file_get_contents(SABRES_PLUGIN_DIR.'/sabres-config-template.php');
      $result=Sabres_WP::instance()->file_put_contents_safe($config_file_path,'e3e74741-ff42-488c-870f-293ab212ab4b',sprintf($template,ABSPATH,SABRES_PLUGIN_DIR,SABRES_PLUGIN_BASE_NAME,SBS_PLUGIN_URL,SBS_MAIN_PLUGIN_FILE,WP_CONTENT_DIR));
      if (is_wp_error($result)) {
        $settings->sabres_config_error=$result->get_error_message();
      }
      else {
        @include_once $config_file_path;
      }
    }
    if ($update_plugin_url && !$settings->should_trigger_activation()) {
      if ($settings->update_plugin_url!='true') {
        $settings->update_plugin_url='true';
        $settings->trigger_hourly_cron_now='true';
      }
    }
  }

}
Check_Sabres_Config::check();
