<?php
// Definitions
define( 'SBS_DISPATCH', true );

// Disable time limit
@ini_set( 'max_execution_time', '0' );
@set_time_limit( 0 );

// Disable memory limit
@ini_set( 'memory_limit', '-1' );

// Variables
$base_path = preg_replace( '/wp-content(?!.*wp-content).*/', '', __DIR__ );

// Load WP
//require_once $base_path . 'wp-load.php';
//
//$res = wp_remote_get('http://www.google.com', array(
//    'method' => 'GET',
//    'timeout' => 10,
//    'redirection' => 5,
//    'httpversion' => '1.0',
//    'sslverify' => false,
//    'blocking' => true,
//    'headers' => array(),
//    'body' => false,
//    'cookies' => array()
//));
//
//die();

// Includes
require_once __DIR__ . '/_inc/transports/curl.php';
require_once __DIR__ . '/_inc/transports/fsock.php';

// CURL
$options = array(
    'timeout' => 10,
    'connect_timeout' => 10,
    'useragent' => "WordPress/4.6.1; http://localhost",
    'protocol_version' => '1.1',
    'redirected' => 0,
    'redirects' => 5,
    'follow_redirects' => 1,
    'blocking' => true,
    'type' => 'GET',
    'filename' => false,
    'auth' => '',
    'proxy' => '',
    'cookies' => '',
    'max_bytes' => '',
    'idn' => 1,
    'transport' => '',
    'verify' => false,
    'verifyname' => false,
    'data_format' => 'query'
    //'data_format' => 'body'
);

$transport_test = SBS_Transport_cURL::test(
    array( 'ssl' => true )
);

if ( $transport_test ) {
    $curl = new SBS_Transport_cURL();

    $response = $curl->request('http://www.morning-sickness.co.il',
        array(),
        null,
        $options
    );

    echo $curl->response_data;
}

$transport_test = SBS_Transport_fsock::test(
    array(
    )
);

if ( $transport_test ) {
    $transport = new SBS_Transport_fsock();

    $response = $transport->request('http://www.google.com',
        array(),
        null,
        $options
    );

    echo $transport->response_data;
}

// Exit
exit;